import 'package:dart_training/src/model/image_data.dart';
import 'package:dart_training/src/question/question1/utility/base_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.g.dart';

typedef ID = int;

@JsonSerializable()
class User extends BaseModel {
  final ID id;
  @JsonKey(name: "first_name")
  final String firstName;
  @JsonKey(name: "last_name")
  final String lastName;
  final String about;
  @JsonKey(name: "image")
  final String imageUrl;
  final String email;

  User(
      {required this.email,
      required this.id,
      required this.firstName,
      required this.lastName,
      required this.about,
      required this.imageUrl});

  String? _fullName;

  String get fullName => _fullName ??= '$firstName $lastName';
  AppImageData? _imageData;

  AppImageData get imageData =>
      _imageData ??= CachedAppImageData(url: imageUrl);

  factory User.fromJson(Json json) => _$UserFromJson(json);

  @override
  Json toJson() => _$UserToJson(this);
}
