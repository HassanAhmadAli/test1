import 'package:dart_training/src/question/question3/get_user.dart';
import 'package:dart_training/src/question/question3/model/user.dart';
import 'package:dart_training/src/question/question3/presentation/user_page.dart';
import 'package:flutter/material.dart';

class UsersList extends StatefulWidget {
  const UsersList({super.key});

  @override
  State<UsersList> createState() => _UsersListState();
}

class _UsersListState extends State<UsersList> {
  List<ID>? usersIDs;

  @override
  void initState() {
    getUsersIDs().then(
      (UsersIDs) => setState(
        () {
          usersIDs = UsersIDs;
        },
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: usersIDs != null
            ? ListView.separated(
                itemCount: usersIDs!.length,
                itemBuilder: (context, index) {
                  final id = index;
                  return ListTile(
                    title: Center(child: Text(id.toString())),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => UserPage(
                            id: id,
                          ),
                        ),
                      );
                    },
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              )
            : const CircularProgressIndicator(),
      ),
    );
  }
}
