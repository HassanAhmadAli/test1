import 'dart:convert';

import 'package:dart_training/src/question/question3/model/user.dart';
import 'package:dart_training/src/utility/assets_manager.dart';
import 'package:dart_training/src/utility/logger.dart';
import 'package:faker/src/faker.dart';
import 'package:flutter/services.dart';

final Map<ID, User> _cachedUserDetails = {};

User? getUserById(ID? id) {
  if (id == null || id < 0) return null;
  if (!_cachedUserDetails.containsKey(id)) {
    _cachedUserDetails[id] = User(
      id: id,
      firstName: faker.person.firstName(),
      lastName: faker.person.lastName(),
      about: faker.lorem.words(3).join(' '),
      imageUrl: faker.image.image(),
      email: faker.internet.email(),
    );
  }
  return _cachedUserDetails[id];
}

List<ID>? _usersIDs;

Future<List<ID>> getUsersIDs() async {
  if (_usersIDs == null) {
    logger.d('loading users IDs');
    String jsonString = await rootBundle.loadString(AssetsManager.users);
    List<dynamic>? dynamics = await json.decode(jsonString);
    _usersIDs = dynamics!.map((entry) => entry.values.first as ID).toList();
    // because here dynamics is considered of type List<Map<String, Iterable<int>>>
    // we could also use
    // _usersIDs = [];
    // for (Map<String, Iterable<ID>> map in dynamics) {
    //   _usersIDs!.addAll(map.values as Iterable<ID>);
    // }
    // but here we know for sure that the iterable is containing only one element
  } else {
    logger.d('users IDs alreay loaded');
  }
  return _usersIDs!;
}
