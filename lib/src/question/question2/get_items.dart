import 'dart:convert';

import 'package:dart_training/src/utility/assets_manager.dart';
import 'package:dart_training/src/utility/logger.dart';
import 'package:faker/src/faker.dart';
import 'package:flutter/services.dart';

List<String>? _categories;

List<String> getCategories() {
  if (_categories == null) {
    logger.d('loading categories');
    _categories = List.generate(
      1000,
      (index) => faker.lorem.words(3).join(' '),
    );
  } else {
    logger.d('categories already loaded');
  }
  return _categories!;
}

List<String>? _venues;

List<String> getVenues() {
  if (_venues == null) {
    logger.d('loading venues');
    _venues = List.generate(
      1000,
      (index) => faker.lorem.words(2).join(' '),
    );
  } else {
    logger.d('venues already loaded');
  }
  return _venues!;
}

List<String>? _languages;

Future<List<String>> get languages async {
  if (_languages == null) {
    logger.d('loading languages');
    final jsonString = await rootBundle.loadString(AssetsManager.languages);
    final decodedList = await json.decode(jsonString);
    _languages = decodedList.cast<String>();
  } else {
    logger.d('languages already loaded');
  }
  return _languages!;
}
