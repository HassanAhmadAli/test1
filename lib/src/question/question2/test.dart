import 'package:dart_training/src/question/question2/get_items.dart';
import 'package:dart_training/src/utility/logger.dart';
import 'package:flutter/foundation.dart';

void q2Test() async {
  //
  if (kDebugMode) {
    logger.i('categories');
    logger.i(getCategories());
    logger.i('venues');
    logger.i(getVenues());
    logger.i('languages');
    logger.i(await languages);
    logger.i(await languages);
  }
}
