import 'package:dart_training/src/question/question1/utility/base_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'availability.freezed.dart';
part 'availability.g.dart';
@freezed
class Availability extends BaseModel with _$Availability {
  const factory Availability({
    required bool online,
    required bool inStore,
  }) = _Availability;
  factory Availability.fromJson(Json json) => _$AvailabilityFromJson(json);
}
