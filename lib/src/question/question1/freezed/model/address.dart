import 'package:dart_training/src/question/question1/utility/base_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'address.freezed.dart';
part 'address.g.dart';
@freezed
class Address extends BaseModel with _$Address {
  const factory Address({
    required String street,
    required String city,
    required String state,
    required String postalCode,
  }) = _Address;
  factory Address.fromJson(Json json) => _$AddressFromJson(json);
}
