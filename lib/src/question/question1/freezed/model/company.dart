import 'package:freezed_annotation/freezed_annotation.dart';

import '../../utility/base_model.dart';
import 'address.dart';
import 'department.dart';

part 'company.freezed.dart';
part 'company.g.dart';

@freezed
class Company extends BaseModel with _$Company {
  const factory Company({
    @JsonKey(name: "is_active") int? isActive,
    @JsonKey(name: "name") required String name,
    required Address? address,
    required DateTime established,
    // here the package 'freezed' generate the required logic to deal with List.fromJson
    required List<Department> departments,
  }) = _Company;

  factory Company.fromJson(Json json) => _$CompanyFromJson(json);
}
