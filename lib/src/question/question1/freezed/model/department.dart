import 'package:dart_training/src/question/question1/freezed/model/availability.dart';
import 'package:dart_training/src/question/question1/utility/base_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'department.freezed.dart';
part 'department.g.dart';

@freezed
class Department extends BaseModel with _$Department {
  const factory Department({
    required String deptId,
    required String name,
    required String manager,
    required double budget,
    int? year,
    Availability? availability,
    @JsonKey(name: "meeting_time") required String meetingTime,
  }) = _Department;

  factory Department.fromJson(Json json) => _$DepartmentFromJson(json);
}
