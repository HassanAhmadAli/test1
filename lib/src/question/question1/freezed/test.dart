import 'package:dart_training/src/utility/logger.dart';

import '../utility/base_model.dart';
import '../utility/company_data.dart';
import 'model/company.dart';

Future<void> q1FreezedTest() async {
  final loadedCompaniesData = await companiesData;

  final companies = BaseModelListHelper.fromJsons(
    jsons: List.generate(loadedCompaniesData.length,
        (index) => loadedCompaniesData[index]['company']),
    fromJson: Company.fromJson,
  );

  logger.i('quick Type');
  for (final company in companies) {
    logger.i(company.toJson());
    final formattedDate = DateHelper.dateFormatter.format(company.established);
    logger.i('formatted date $formattedDate');
  }
}
