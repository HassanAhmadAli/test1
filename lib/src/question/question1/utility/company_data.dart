import 'dart:convert';

import 'package:dart_training/src/question/question1/utility/base_model.dart';
import 'package:dart_training/src/utility/assets_manager.dart';
import 'package:dart_training/src/utility/logger.dart';
import 'package:flutter/services.dart';

List<Json>? _company;

Future<List<Json>> get companiesData async {
  if (_company == null) {
    logger.d('loading companies data');
    _company = [];
    String jsonString1 = await rootBundle.loadString(AssetsManager.test1);
    String jsonString2 = await rootBundle.loadString(AssetsManager.test2);
    _company!.add(await json.decode(jsonString1));
    _company!.add(await json.decode(jsonString2));
  }
  {
    logger.d('companies data already loaded');
  }
  return _company!;
}
