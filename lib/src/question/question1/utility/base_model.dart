import 'package:intl/intl.dart';
typedef Json = Map<String, dynamic>;
abstract class BaseModel {
  const BaseModel();
  Json toJson();
}
mixin BaseModelListHelper {
  static List<T> fromJsons<T extends BaseModel>({
    required List<Json> jsons,
    required T Function(Json) fromJson,
  }) {
    return List.generate(
      jsons.length,
      (index) => fromJson(jsons[index]),
    );
  }
  static List<Json> listToJson<T extends BaseModel>(List<T> list) =>
      List.generate(list.length, (index) => list[index].toJson());
}
class DateHelper {
  static final dateFormatter = DateFormat('d-MMM, y');
  // Static method to format the date
  static String formatDate(DateTime date) {
    return dateFormatter.format(date);
  }
}
