import 'package:dart_training/src/question/question1/quicktype/test.dart';

import 'freezed/test.dart';
import 'json_serializable/test.dart';
import 'manual/test.dart';

void q1Test() {
  for (final function in [
    q1QuickTypeTest,
    q1FreezedTest,
    q1JsonSerializableTest,
    q1ManualTest
  ]) {
    function();
  }
}
