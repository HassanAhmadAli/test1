import 'package:dart_training/src/question/question1/utility/base_model.dart';
class Address extends BaseModel {
  final String street;
  final String city;
  final String state;
  final String postalCode;
  Address({
    required this.street,
    required this.city,
    required this.state,
    required this.postalCode,
  });
  Address copyWith({
    String? street,
    String? city,
    String? state,
    String? postalCode,
  }) =>
      Address(
        street: street ?? this.street,
        city: city ?? this.city,
        state: state ?? this.state,
        postalCode: postalCode ?? this.postalCode,
      );
  factory Address.fromJson(Json json) => Address(
        street: json["street"],
        city: json["city"],
        state: json["state"],
        postalCode: json["postalCode"],
      );
  @override
  Json toJson() => {
        "street": street,
        "city": city,
        "state": state,
        "postalCode": postalCode,
      };
}
