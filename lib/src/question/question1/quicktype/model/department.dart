import 'package:dart_training/src/question/question1/quicktype/model/availability.dart';
import 'package:dart_training/src/question/question1/utility/base_model.dart';
class Department extends BaseModel {
  final String deptId;
  final String name;
  final String manager;
  final double budget;
  final int? year;
  final Availability? availability;
  final String meetingTime;
  Department({
    required this.deptId,
    required this.name,
    required this.manager,
    required this.budget,
    this.year,
    this.availability,
    required this.meetingTime,
  });
  Department copyWith({
    String? deptId,
    String? name,
    String? manager,
    double? budget,
    int? year,
    Availability? availability,
    String? meetingTime,
  }) =>
      Department(
        deptId: deptId ?? this.deptId,
        name: name ?? this.name,
        manager: manager ?? this.manager,
        budget: budget ?? this.budget,
        year: year ?? this.year,
        availability: availability ?? this.availability,
        meetingTime: meetingTime ?? this.meetingTime,
      );
  factory Department.fromJson(Json json) => Department(
        deptId: json["deptId"],
        name: json["name"],
        manager: json["manager"],
        budget: json["budget"],
        year: json["year"],
        availability: json["availability"] == null
            ? null
            : Availability.fromJson(json["availability"]),
        meetingTime: json["meeting_time"],
      );
  @override
  Json toJson() => {
        "deptId": deptId,
        "name": name,
        "manager": manager,
        "budget": budget,
        "year": year,
        "availability": availability?.toJson(),
        "meeting_time": meetingTime,
      };
}
