import 'package:dart_training/src/question/question1/quicktype/model/address.dart';
import 'package:dart_training/src/question/question1/quicktype/model/department.dart';
import 'package:dart_training/src/question/question1/utility/base_model.dart';

class Company extends BaseModel {
  final int? companyIsActive;
  final String name;
  final Address? address;
  final DateTime established;
  final List<Department> departments;
  final int? isActive;

  Company({
    this.companyIsActive,
    required this.name,
    required this.address,
    required this.established,
    required this.departments,
    this.isActive,
  });

  Company copyWith({
    int? companyIsActive,
    String? name,
    Address? address,
    DateTime? established,
    List<Department>? departments,
    int? isActive,
  }) =>
      Company(
        companyIsActive: companyIsActive ?? this.companyIsActive,
        name: name ?? this.name,
        address: address ?? this.address,
        established: established ?? this.established,
        departments: departments ?? this.departments,
        isActive: isActive ?? this.isActive,
      );

  factory Company.fromJson(Json json) => Company(
        companyIsActive: json["is_active"],
        name: json["name"],
        address:
            json["address"] == null ? null : Address.fromJson(json["address"]),
        established: DateTime.parse(json["established"]),
        // the site provided other method
        departments: List<Department>.from(
            json["departments"].map((x) => Department.fromJson(x))),
        isActive: json["isActive"],
      );

  @override
  Json toJson() => {
        "is_active": companyIsActive,
        "name": name,
        "address": address?.toJson(),
        "established": established.toUtc().toIso8601String(),
        "departments": List<dynamic>.from(departments.map((x) => x.toJson())),
        "isActive": isActive,
      };
}
