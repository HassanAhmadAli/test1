import 'package:json_annotation/json_annotation.dart';
import '../../utility/base_model.dart';
import 'address.dart';
import 'department.dart';

part 'company.g.dart';

@JsonSerializable()
class Company extends BaseModel with BaseModelListHelper {
  @JsonKey(name: "is_active")
  int? isActive;
  String name;
  Address? address;
  DateTime established;
  List<Department> departments;

  Company({
    this.isActive,
    required this.name,
    required this.address,
    required this.established,
    required this.departments,
  });

  factory Company.fromJson(Json json) => _$CompanyFromJson(json);

  @override
  Json toJson() => _$CompanyToJson(this);
}
