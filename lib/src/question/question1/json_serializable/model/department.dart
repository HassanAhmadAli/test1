import 'package:dart_training/src/question/question1/freezed/model/availability.dart';
import 'package:dart_training/src/question/question1/utility/base_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'department.g.dart';

@JsonSerializable()
class Department extends BaseModel with BaseModelListHelper {
  String deptId;
  String name;
  String manager;
  double budget;
  int? year;
  Availability? availability;
  @JsonKey(name: "meeting_time")
  String meetingTime;

  Department({
    required this.deptId,
    required this.name,
    required this.manager,
    required this.budget,
    this.year,
    this.availability,
    required this.meetingTime,
  });

  factory Department.fromJson(Json json) => _$DepartmentFromJson(json);

  @override
  Json toJson() => _$DepartmentToJson(this);
}
