import '../../utility/base_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'address.g.dart';
@JsonSerializable()
class Address extends BaseModel with BaseModelListHelper {
  String street;
  String city;
  String state;
  String postalCode;
  Address({
    required this.street,
    required this.city,
    required this.state,
    required this.postalCode,
  });
  factory Address.fromJson(Json json) => _$AddressFromJson(json);
  @override
  Json toJson() => _$AddressToJson(this);
}
