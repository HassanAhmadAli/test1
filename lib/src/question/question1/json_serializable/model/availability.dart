import 'package:dart_training/src/question/question1/utility/base_model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'availability.g.dart';
@JsonSerializable()
class Availability extends BaseModel with BaseModelListHelper {
  bool online;
  bool inStore;
  Availability({
    required this.online,
    required this.inStore,
  });
  factory Availability.fromJson(Json json) => _$AvailabilityFromJson(json);
  @override
  Json toJson() => _$AvailabilityToJson(this);
}
