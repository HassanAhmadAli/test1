import '../../utility/base_model.dart';

class Availability extends BaseModel {
  final bool online;
  final bool inStore;

  Availability({
    required this.online,
    required this.inStore,
  });

  factory Availability.fromJson(Json json) => Availability(
        online: json["online"],
        inStore: json["inStore"],
      );

  @override
  Json toJson() => {
        "online": online,
        "inStore": inStore,
      };
}
