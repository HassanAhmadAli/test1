import 'package:dart_training/src/question/question1/manual/model/availability.dart';
import 'package:dart_training/src/question/question1/utility/base_model.dart';

class Department extends BaseModel {
  final String deptId;
  final String name;
  final String manager;
  final double budget;
  final int? year;
  final Availability? availability;
  final String meetingTime;
  static const meetingTimeJsonKey = "meeting_time";

  Department({
    required this.deptId,
    required this.name,
    required this.manager,
    required this.budget,
    this.year,
    this.availability,
    required this.meetingTime,
  });

  factory Department.fromJson(Json json) => Department(
        deptId: json["deptId"],
        name: json["name"],
        manager: json["manager"],
        budget: json["budget"],
        year: json["year"],
        availability: json["availability"] == null
            ? null
            : Availability.fromJson(json["availability"]),
        meetingTime: json[meetingTimeJsonKey],
      );

  @override
  Json toJson() => {
        "deptId": deptId,
        "name": name,
        "manager": manager,
        "budget": budget,
        "year": year,
        "availability": availability?.toJson(),
        meetingTimeJsonKey: meetingTime,
      };
}
