import '../../utility/base_model.dart';

class Address extends BaseModel {
  final String street;
  final String city;
  final String state;
  final String postalCode;

  Address({
    required this.street,
    required this.city,
    required this.state,
    required this.postalCode,
  });

  factory Address.fromJson(Json json) => Address(
        street: json["street"],
        city: json["city"],
        state: json["state"],
        postalCode: json["postalCode"],
      );

  @override
  Json toJson() => {
        "street": street,
        "city": city,
        "state": state,
        "postalCode": postalCode,
      };
}
