import 'package:dart_training/src/question/question1/test.dart';
import 'package:dart_training/src/question/question2/test.dart';
import 'package:dart_training/src/question/question3/presentation/users_list.dart';
import 'package:flutter/material.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    for (final test in [
      q1Test,
      q2Test,
    ]) {
      test();
    }

    return MaterialApp(
      routes: {'/': (_) => const UsersList()},
    );
  }
}
