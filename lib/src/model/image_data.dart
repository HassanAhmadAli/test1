import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

/// we can use freezed union instead of sealed here ,
/// but this way is better for performance
/// as it allows for lazy loading of ImageProvider
sealed class AppImageData {
  // fields
  ImageProvider? _imageProvider;

  // getters
  ImageProvider get imageProvider;

  // default contructor
  AppImageData() : _imageProvider = null;
}

class NetworkAppImageData extends AppImageData {
  final String url;

  NetworkAppImageData({
    required this.url,
  });

  @override
  ImageProvider get imageProvider => _imageProvider ??= NetworkImage(url);
}

class CachedAppImageData extends AppImageData {
  final String url;

  CachedAppImageData({
    required this.url,
  });

  @override
  ImageProvider get imageProvider =>
      _imageProvider ??= CachedNetworkImageProvider(url);
}

class FileWithUrlAppImageData extends AppImageData {
  final String url;
  final String path;
  late final bool _fileExist;
  final File _file;

  FileWithUrlAppImageData({
    required this.url,
    required this.path,
  }) : _file = File(path) {
    _file.exists().then((fileExist) {
      _fileExist = fileExist;
    });
  }

  @override
  ImageProvider get imageProvider {
    if (_imageProvider == null) {
      if (_fileExist) {
        _imageProvider = FileImage(_file);
      } else {
        _imageProvider = CachedNetworkImageProvider(url);
      }
    }
    return _imageProvider!;
  }
}

class AssetAppImageData extends AppImageData {
  final String assetName;

  AssetAppImageData({
    required this.assetName,
  });

  @override
  ImageProvider get imageProvider => _imageProvider ??= AssetImage(assetName);
}

class FileAppImageData extends AppImageData {
  final String path;
  final File _file;

  FileAppImageData({
    required this.path,
  }) : _file = File(path);

  @override
  ImageProvider get imageProvider => _imageProvider ??= FileImage(_file);
}
